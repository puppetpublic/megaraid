# class for MegaRAID monitoring via Nagios
class megaraid {
  
  # Similar version can be found in AFS, though installing the
  # package locally removes the AFS dependency:
  # /afs/ir/product/firmware/dell/misc/MegaCli/8.07.10/MegaCli64 
  $megacli_pkg = $::osfamily? {
    'Debian' => 'megacli',
    'RedHat' => 'MegaCli',
    default  => 'megacli',
  }
  package { $megacli_pkg: ensure => installed }

  # ensure that the dir exists in the event of a Puppet race condition
  $nagios_plugin_path = [ '/usr/lib/nagios', '/usr/lib/nagios/plugins' ]
  file { $nagios_plugin_path:
    ensure => directory,
  }
  
  # bash megaraid health check script
  file { '/usr/lib/nagios/plugins/check_megaraid':
    ensure  => present,
    source  => 'puppet:///modules/megaraid/usr/lib/nagios/plugins/check_megaraid',
    require => File [ $nagios_plugin_path ],
    mode    => '0755',
  }
  file { '/etc/remctl/conf.d/megaraid':
    ensure => present,
    source => 'puppet:///modules/megaraid/etc/remctl/conf.d/megaraid',
  }

  # MegaCli64 wrapper; could use some improvements but still useful
  file { '/opt/MegaRAID/MegaCli/megacli_wrapper':
    ensure  => present,
    source  => 'puppet:///modules/megaraid/opt/MegaRAID/MegaCli/megacli_wrapper',
    require => Package[ $megacli_pkg ],
  }

  # filter-syslog configuration for remote probes
  file { '/etc/filter-syslog/megaraid':
    source => 'puppet:///modules/megaraid/etc/filter-syslog/megaraid',
  }

}
